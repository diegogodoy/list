//
// Created by diego on 11/1/15.
// Implements a linked list, using template, smart_ptr
//

#include <memory>
#include <exception>


#ifndef LIST_LIST_HPP
#define LIST_LIST_HPP

using namespace std;

namespace ED{

template <typename T>
class list{

	struct node{
	public:
		node(T obj) : obj(obj), next(nullptr){ };
		T obj;
		shared_ptr<node> next;
	};

	size_t mSize = 0;

	shared_ptr<node> first, last;

	size_t search(T& t) const {
		size_t i;
		shared_ptr<node> temp;
		for (temp = first; ; i++, temp = temp->next) {
			if(temp->obj == t)
				return i;
			if(temp->next == nullptr)
				return -1;
		}
	}

	public:

	class iterator{

		private:
			iterator(shared_ptr<node> n):current(n){};

		public:
			iterator():current(nullptr){};

			T &operator*(){
				return retrieve();
			}

			iterator &operator++(){
				current = current->next;
				return *this;
			} //++i

			iterator &operator++(int){//i++
				auto old = *this;
				++( *this );
				return old;
			}

			bool operator==(iterator rhs){
				return current == rhs.current;
			}

			bool operator!=(iterator rhs){
				return (current != rhs.current);
			}


		protected:
			shared_ptr<node> current;
			T & retrieve(){
				return current->obj;
			}
			friend class list<T>;
	};

	list() = default;

	~list() = default;

	size_t size() const {
		return mSize;
	}

	void insert(T t, uint position){
		if(position > mSize){
			throw "error: Posição é maior que tamanho da lista";
		}
		auto p  = make_shared<node>(node(t)); //equivalente a malloc, porem com smart pointer.
		uint i = 0;
		if(position == 0){
			p->next = first;
			first = p;
			mSize++;
			return ;
		}

		for(auto temp = first; i <= position; i++, temp = temp->next){
			if(i == position - 1){
				p->next = temp->next;
				temp->next = p;
				break;
			}
			if( i == (mSize - 1) ){
				temp->next = p;
				last = p;
			}
		}
		mSize++;
	}

	void erase(long int position){
		if(position > mSize || position < 0){
			throw ;
		}
		shared_ptr<node> temp;
		long int i = 0;
		if(position == 0){
			first = first->next;
			mSize--;
			return ;
		}
		for(temp = first; i <= position; i++, temp = temp->next){
			if (i == (mSize-2) && position - 1 == i){
				temp->next = nullptr;
				last = temp;
				break;
			}
			if( i == position - 1){
				auto a = temp->next;
				temp->next= a->next;
			}
		}
		mSize--;
	}

	void push_back(T t){
		insert(t, mSize);
	}

	void push_front(T t){
		insert(t, 0);
	}

	void pop_back(){
		erase(mSize-1);
	}

	void pop_front(){
		erase(0);
	}

	size_t max_size(){
		return  mSize;
	}

	iterator begin(){
		return mSize ? iterator(first) : iterator(nullptr);
	}

	iterator end(){
		auto ret = iterator(last);
		return ret;
	}
};
}
#endif //LIST_LIST_HPP
