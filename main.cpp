#include "list.hpp"
#include <iostream>

using namespace ED;
using namespace std;

int main(){
	list<int> lista;
	lista.push_front(4);
	lista.push_front(5);
	lista.push_front(4);
	lista.push_front(8);
	lista.push_front(9);
	for(auto& i: lista)
		cout << i << endl;
}
